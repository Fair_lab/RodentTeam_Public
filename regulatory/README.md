# Regulatory

Animal research at the DCAN lab is governed by a collection of laws and guidelines from various organizations. These are in place to ensure animal welfare and good science.

---
:monkey_face: = Covers monkey research
:mouse: = Covers rodent research

- Institutional Animal Care and Use Committee (IACUC):monkey_face: :mouse:
    - Reviews the animal care and use program and ensures compliance every six months
    - Self-regulating body for animal research on OHSU's behalf 
- U.S. Department of Agriculture (USDA) and the Animal Welfare Act (AWA) :monkey_face:
    - Covers all mammals except for birds, mice, and rats bred for research, horses not used for research, livestock for agriculture
    - Enforced by the Animal Welfare Regulations (AWR)
    - [Animal Care Policy Manual](https://www.aphis.usda.gov/aphis/ourfocus/animalwelfare/sa_publications/ct_publications_and_guidance_documents) explains how the AWR should be interpreted

- U.S. Public Health Service (PHS) and the Office of Laboratory Animal Welfare (OLAW) :monkey_face: :mouse:
    - Covers all vertebrate species used for research
    - OLAW monitors compliance
    - [PHS Policy on Humane Care and Use of Laboratory Animals](https://olaw.nih.gov/policies-laws/phs-policy.htm)
    - [The Guide](https://grants.nih.gov/grants/olaw/guide-for-the-care-and-use-of-laboratory-animals.pdf) for the Care and Use of Laboratory Animals
- AAALAC International :monkey_face: :mouse:
    - Accredits animal programs 
    - Vists program sites every 3 years

