# Rodent Team - Public

This repository provides publicly available information about the rodent projects at the [Developmental Cognition & Neuroimaging Lab](http://www.ohsu.edu/xd/education/schools/school-of-medicine/departments/basic-science-departments/behn/people/labs/fair-neuroimaging-lab/) located at [Oregon Health & Science University](http://www.ohsu.edu/xd/research/). 

## Directory 

- Anesthesia 
    - dexmedetomidine_isoflurane
- IACUC
- IL6_Maternal_Inflammation
- MRIAcquisition
- MSD
- microPET
- opto-fMRI
    - MRIAcquisition


## Links

* [Fair Neuroimaging Lab](https://www.ohsu.edu/xd/education/schools/school-of-medicine/departments/basic-science-departments/behn/people/labs/fair-neuroimaging-lab/) - Lab Website
* [DCAN Labs Github](https://github.com/DCAN-Labs) - Lab github
* [AIRC](https://www.ohsu.edu/xd/research/centers-institutes/airc/) - Advanced Imaging Research Center 
* [ACC](https://www.ohsu.edu/xd/research/research-cores/advanced-computing-center/) - Advanced Computing Center
* [HSR](http://www.ohsu.edu/xd/research/research-cores/histopathology/index.cfm) - Histopathology Shared Resource
* [ALMC](http://www.ohsu.edu/xd/research/research-cores/almc/) - Advanced Light Microscopy Core
* [SAIRC](http://www.ohsu.edu/xd/research/research-cores/small-animal-research-imaging-core/) - Small Animal Research Imaging Core
* [BEHN](http://www.ohsu.edu/xd/education/schools/school-of-medicine/departments/basic-science-departments/behn/) - Department of Behavioral Neuroscience
* [OHSU](https://www.ohsu.edu/xd/) - Oregon Health & Science University

## Rodent Team Contacts

* **Alina Goncharova** - *Research Assistant* - [email](gonchara@ohsu.edu)
* **Andrea Hardin** - *Phd Student* - [email](hardinan@ohsu.edu)
* **Anandakumar Shunmugavel** - *Postdoc* - [email](shunmuga@ohsu.edu)
* **Damien Fair, PA-C, Ph.D.** - *Associate Professor* - [email](faird@ohsu.edu)
* **Greg Conan** - *Volunteer* - [email](conan@ohsu.edu)
* **Martin Pike, Ph.D.** - *Associate Professor - AIRC* - [email](pikema@ohsu.edu)

## Rodent Team Alumni

* **Brian Mills, Ph.D.** - *Postdoc at Stanford* - [email](millsb@stanford.edu)
