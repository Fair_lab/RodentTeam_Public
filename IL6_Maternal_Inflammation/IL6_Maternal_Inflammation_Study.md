# IL6 Maternal Inflammation Study

This study was started and lead by Brian Mills as his thesis work. Ongoing work continues to examine the alterations in rats that were exposed to chronically elevated levels of IL-6 in the prenatal environment. 

## Behavior
*Add info*

## Imaging
*Add info*


## Histology

*Add info*

## Cytokine Concentration

*Add info*


## Links

* [Add Links](website) - Explain link



## Authors

* **Alina Goncharova** - *Initial work* - [email](gonchara@ohsu.edu)
* **Contributing Author** - *What was added* - [contact info](email@company.com)

