# Opto-fMRI Prep

## General Information


## Set Up

### Baphy Set Up

1. Plug in laptop power, login
2. Open Matlab
    - Under the Current Folder window, navigate to `C:baphy`
    - Type `baphy` in the Command Window
3. Connect NI DAQ USB cable to laptop (DAQ is on when blue light next to USB is on)
4. Plug in LED driver power
5. Remove red LED cover cap (dont lose it)
6. Test LED
    - Switch LED driver from TRIG (tigger) to CW (continuous wave)
    - Check blue LED light is on (Thor Labs M470F3) *Not on*
    - Switch LED driver from CW back to TRIG (LED should be off now)
7. Connect fiber optic cable to LED (Thor Labs M470F3)
8. Connect TTL signal cable "T.STAMP/SPEKTROMETER" in the back MRI room and National Instrument end
9. BaphyMainGui - Choose tester and animal from list
    Update necessary info:
    - Tester: `your name`
    - Ferret: `Subject Name`
    - Module": `Reference Target`
    - Physiology: `Yes -- Passive`
    - Hardware Setup: `1: FNL 1`
    - Click Continue
10. Sanity Check Settings for the appropriate parameters 
    - Click Import Parameters 
    - Browse` C:Data`
    - Pick stimulation protocol you want
    - Click Ok
    - Click Import
    - Click Start
    - In baphy, click `Start` and append the filename, then click OK.

### Scanner Set Up

1. Insert volume coil into the back-end of the bore. Secure screw to hold it in place.
2. Insert cable for the surface coil in through the back of the bore.
3. Switch out to the correct aluminium plate that holds the animal halfpipe. 
4. Connect surface coil cable to surface coil (Tom's Rat Surface Coil)
5. Set up the backend of the 12T to the AIRC cross-coil configuration.

### Animal Monitoring Set Up

1. Move Respiration/Temperature monitor to the MRI suite. Plug it into the outlet, connect the respiration pillow to a extention tube.
2. 
3. 


### Animal Set Up

1. Turn on the oxygen tank located in the back room to the left of the 12T.
2. Set up the bench-top induction chamber so that the oxygen and vacuum cables are switched on correctly. 
3. Turn oxygen to 2 L/min. Turn on vacuum.
4. Transfer animal into bench-top iso chamber. Turn isoflurane on to 3%.
5. Wait until animal falls asleep



## System Control
System control is run from the main computer (Monitor HP LP2065) in the 12T room. 


Tripilot
Fieldmap
Presswaterline
T1
T2
EPI 1 rep
EPI - 20
EPI - 20
EPI rest 
EPI - 15
EPI - 15
EPI rest
EPI - 10
EPI - 10 
EPI rest 
EPI - 5 
EPI -5 
REV Tripilot
REV EPI






 Edit scan
 Research, Trigger out module, , Trigger out Delay 100.0 ms
 
 
 
 
 ### Run Opto-fMRI Parameters
1. In ParaVision, change the "Trigger Out Moddle" to "On". Set Trigger Out Delay to 100 ms. 
2. In baphy (if not done yet), click `Start` and append the filename, then click OK.
3. When ready to start (before you start the EPI) go to malab and hit the spacebar in the command window. Then start the EPI scan. 
- Watch and listen for the BAPHY TTL countdown and magnet to start at the same time. (If they don't start at the same time, record the offset in a logbook, all stimuli will be offset)
4. When completed it will ask "Continue the experiment?" click "No"

