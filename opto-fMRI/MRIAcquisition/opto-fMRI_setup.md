# Setting up for and running the opto-fMRI parameters

## Before Scan Date

- Virus Infusion: 4-6 weeks prior to scan
- Cannula placement: at least 3 days before scan
- DCM Animal Transfer: Complete at least 3 days before date of the scan
- Baphy set up: day before

### Baphy Set up - before scan:

1. Get the PC laptop from DCAN Lab named "MACC056" and logon
2. Add new animals that will be scanned to the list located at `C:\baphy\Config\BaphyConfigPath.m`
    - Navigate to `C:\baphy\Config\` right click on `BaphyConfigPath` and click Open with Notepad.
    - Add animals to the ANIMAL_LIST and staff to the EXPERIMENTER_LIST. Click file, save, close.

3. Configure the new animal
    - Open matlab 
    - Under the Current Folder window, navigate to `C:baphy`
    - Type `baphy` in the Command Window

4. BaphyMainGui
Update necessary info:
    - Tester: `your name`
    - Ferret: `Subject Name`
    - Module": `Reference Target`
    - Physiology: `Yes -- Passive`
    - Hardware Setup: `1: FNL 1`
    - Click Continue

5. Configure your LED triggering experimental design
- Click Import Parameters 
- Browse` C:Data`
- Pick stimulation protocol you want
- Click Ok
- Click Import
- Click Start

```
for 20-second stimulus
BaphyRefTarGui
    - Trial Object: `RefTarOpt`
    - Online Waveform: `No`
    - Repetition: `1`
    - Trial Block: `1`
    - Continuous Training: `No`

Trial Object Parameters
    - OveralldB: `65`
    - RelativeTarRefdB: `0`
    - LighPulseRate: `20`
    - LightPulseDuration: `0.005`
    - LightPulseShift:`0`
    - LighEpoch: `Sound`
    - LighBurstDuration: `0.0`
    - LightBurstGap: `0.0` 
    - LightConditions: `Sparse`
    - SparseGapMin: `2`
    - SparseGapMax: `2`
    - Click Ok
*I would keep the burst parameters fixed at 0 if you don't want to use them. 
And if you are using the burst protocol, I would not use the sparse trials setting. We didn't really work through the interactions. 
stephen*

Behaviour Parameters - 
    - Behaviour Control: `MRIPassive`

Parameters - 
    - TR: `2.5`
    - PreBlockSilence: `0`
    - ScanTTLValue: `0`
    - DelayAfterScanTTL: `0`
    - InitTTLCount: `7`
    - Click Ok

Reference - 
    -`Silence`
    - PreStimSilence: `0.1`
    - PostStimSilence: `0.1`
    - SamplingRate: `1000`
    - RefRepCount: `1`
    - Duration: `19.5`
    - StimPerRep: `17`

Target - 
    - `None`
```

### Hardware Setup - before scan:

1. Plug in laptop power, login
2. Open Matlab
    - Under the Current Folder window, navigate to `C:baphy`
    - Type `baphy` in the Command Window
3. Connect NI DAQ USB cable to laptop (DAQ is on when blue light next to USB is on)
4. Plug in LED driver power
5. Remove red LED cover cap (dont lose it)
6. Test LED
    - Switch LED driver from TRIG (tigger) to CW (continuous wave)
    - Check blue LED light is on (Thor Labs M470F3) *Not on*
    - Switch LED driver from CW back to TRIG (LED should be off now)
7. Connect Arduino (mimics MRI TTL signal) USB to laptop and cable to TTL signal cable that usually connects to the "T.STAMP/SPEKTROMETER" cable.

### Test - before scan:

1. On the laptop, open matlab 
    - Under the Current Folder window, navigate to `C:baphy`
    - Type `baphy` in the Command Window
2. BaphyMainGui - Choose tester and animal from list
Update necessary info:
    - Tester: `your name`
    - Ferret: `Subject Name`
    - Module": `Reference Target`
    - Physiology: `Yes -- Passive`
    - Hardware Setup: `1: FNL 1`
    - Click Continue
3. Sanity check settings
    - In baphy, click `Start` and append the filename, then click OK.
4. When ready to start go to malab and hit the spacebar in the command window. 
5. The Arduino will mimic the start of the EPI scan 
    - Watch for the BAPHY TTL countdown and LED.
6. When completed it will ask "Continue the experiment?" click "No"


## Day of Scan

### Hardware Setup - day of scan:

1. Plug in laptop power, login
2. Open Matlab
    - Under the Current Folder window, navigate to `C:baphy`
    - Type `baphy` in the Command Window
3. Connect NI DAQ USB cable to laptop (DAQ is on when blue light next to USB is on)
4. Plug in LED driver power
5. Remove red LED cover cap (dont lose it)
6. Test LED
    - Switch LED driver from TRIG (tigger) to CW (continuous wave)
    - Check blue LED light is on (Thor Labs M470F3) *Not on*
    - Switch LED driver from CW back to TRIG (LED should be off now)
7. Connect fiber optic cable to LED (Thor Labs M470F3)
8. Connect TTL signal cable "T.STAMP/SPEKTROMETER" in the back MRI room and National Instrument end.

### Baphy Setup - day of scan:

1. On the laptop, open matlab 
    - Under the Current Folder window, navigate to `C:baphy`
    - Type `baphy` in the Command Window
2. BaphyMainGui - Choose tester and animal from list
Update necessary info:
    - Tester: `your name`
    - Ferret: `Subject Name`
    - Module": `Reference Target`
    - Physiology: `Yes -- Passive`
    - Hardware Setup: `1: FNL 1`
    - Click Continue
3. Sanity Check Settings for the appropriate parameters 
    - Click Import Parameters 
    - Browse` C:Data`
    - Pick stimulation protocol you want
    - Click Ok
    - Click Import
    - Click Start
    - In baphy, click `Start` and append the filename, then click OK.

### Run Opto-fMRI Parameters - day of scan:
1. In ParaVision, change the "Trigger Out Moddle" to "On". Set Trigger Out Delay to 100 ms. 
2. In baphy (if not done yet), click `Start` and append the filename, then click OK.
3. When ready to start (before you start the EPI) go to malab and hit the spacebar in the command window. Then start the EPI scan. 
- Watch and listen for the BAPHY TTL countdown and magnet to start at the same time. (If they don't start at the same time, record the offset in a logbook, all stimuli will be offset)
4. When completed it will ask "Continue the experiment?" click "No"

